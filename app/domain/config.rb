# frozen_string_literal: true

require 'discordrb'
require 'yaml'

CONFIG = YAML.load_file('settings.yaml')

class Config
  def self.load(name)
    CONFIG[name]
  end
end
