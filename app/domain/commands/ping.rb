# frozen_string_literal: true

require 'discordrb'

class Ping
  def main!(event)
    event.send_message 'pong'
  end

  def description
    "This is a test command to see if I'm still alive"
  end
end
