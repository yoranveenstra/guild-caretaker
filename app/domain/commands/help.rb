# frozen_string_literal: true

require 'discordrb'

class Help
  def main!(event)
    event.send_embed(nil, embed)
  end

  def description
    'This will show the help menu, it lists all the possible commands ' \
    'you can give'
  end

  private

  def embed
    embed = Discordrb::Webhooks::Embed.new(
      title: 'Here are the possible commands you can give me',
      description: embed_description
    )

    embed
  end

  def embed_description
    embed_description = ''
    commands.each do |command|
      command_name = File.basename(command, '.*')
      command_description = Object.const_get(command_name.capitalize).new.description
      embed_description += "**#{Config.load('prefix')}#{command_name}**```#{command_description}```\n"
    end
    embed_description
  end

  def commands
    Dir["#{File.dirname(__FILE__)}/**/*.rb"]
  end
end
