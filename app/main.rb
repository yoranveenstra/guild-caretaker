# frozen_string_literal: true

require 'discordrb'

class Main
  attr_accessor :token, :commands

  def initialize(token:)
    @token = token
  end

  def main!
    bot = Discordrb::Bot.new(token: @token)
    setup!

    bot.message do |event|
      execute_commands!(event) if event.content[0] == Config.load('prefix')
    end

    bot.run unless ENV['ENV'] == 'testing'
  end

  private

  def setup!
    load_commands!
  end

  def load_commands!
    @commands = Dir["#{File.dirname(__FILE__)}/domain/commands/**/*.rb"].map do |file|
      require File.absolute_path(file)
      File.basename(file, '.*')
    end
  end

  def execute_commands!(event)
    if @commands.include? message(event)
      Object.const_get(message(event).capitalize).new.main!(event)
    else
      event.send_temporary_message Config.load('no_command_error'), Config.load('temp_message_time')
    end
  end

  def message(event)
    event.content.downcase.slice(1..)
  end
end
